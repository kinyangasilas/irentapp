function loader(page) 
{
let Access =isLogged();
	if(Access.status==true){
	 $.ajax({
         url: page,
         headers: {
        'Authorization':'Bearer '+Access.accessToken,
        'X-CSRF-TOKEN':'xxxxxxxxxxxxxxxxxxxx',
        'Content-Type':'application/json'
    	},
         data: '0',
         cache: false,
         timeout: 600000, 
      	beforeSend:function(request) {
      	//sessionStorage.setItem("accessToken", accessToken);
       // localStorage.clear();localise page to one
      	},     
        success: function (data) {
        $( "#content" ).html( data );
        },
        error: function (e) {
          //console.log(e);
        }
    });
	}else{
	
	alert("Kindly log in...Loader")
	}
}

function isLogged(){
let Access = {};
if ( typeof sessionStorage.accessToken != "undefined") {
	Access["accessToken"]=sessionStorage.accessToken;
	//checked if logged in
	Access["status"]=true;
	}
	return Access;
}
function Add(url,data){
let Access =isLogged();
	if(Access.status==true){
	 $.ajax({
	 	method:"POST",
	 	url: url,
         headers: {
        'Authorization':'Bearer '+Access.accessToken,
        'X-CSRF-TOKEN':'xxxxxxxxxxxxxxxxxxxx',
        'Content-Type':'application/json'
    	},
         data: JSON.stringify(data),
         cache: false,
         timeout: 600000, 
      	beforeSend:function(request) {
      	//sessionStorage.setItem("accessToken", accessToken);
       // localStorage.clear();localise page to one
      	},     
        success: function (data) {
        let apiRespose =data;
        $(".alert").removeClass("alert-danger");
        $(".alert").addClass("alert-success");
        $(".alert" ).html(apiRespose.message);        
        $("form")[0].reset();
        },
        error: function (e) {
          let apiRespose =JSON.parse(e.responseText);
           $(".alert").removeClass("alert-success");
          $(".alert").addClass("alert-danger");
		  $(".alert" ).html(apiRespose.message); 
        }
    });
	}else{
  //not logged	
	alert("Kindly log in...69")
	}
}